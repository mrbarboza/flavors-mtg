package scryfall

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

const BASE_URL = "https://api.scryfall.com"

type ApiResponse struct {
	FlavorText string `json:"flavor_text"`
}

func GetFlavorText() string {
	flavorTextURI := "/cards/random?q=new:flavor"

	resp, err := http.Get(BASE_URL + flavorTextURI)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}

	data := ApiResponse{}
	if err := json.Unmarshal(body, &data); err != nil {
		fmt.Printf("Error: %v\n", err)
	}

	return data.FlavorText
}
