package main

import "gitlab.com/mrbarboza/flavors-mtg/cmd"

func main() {
	cmd.Execute()
}
